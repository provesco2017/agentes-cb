/**
 * Created by jaime on 05/07/17.
 */
app.controller('lectorController',function ($scope,$cordovaBarcodeScanner,$ionicPopup,$cordovaDevice,$http,$cordovaGeolocation,$rootScope,$cordovaNetwork) {


  $scope.leerCodigo=function () {
          /*var identificador="6be5b9c46e585a48";
          var lat=1;
          var long=1;
          var id_cliente=628;*/

          var identificador = $cordovaDevice.getUUID();
          $cordovaBarcodeScanner.scan().then(function(ImagenScan){
          var posOptions = {timeout: 10000, enableHighAccuracy: false};
          $cordovaGeolocation.getCurrentPosition(posOptions).then(function (position) {
          var lat  = position.coords.latitude
          var long = position.coords.longitude
          var id_cliente=ImagenScan.text;

          json={
            'uuid':identificador,
            'latitud':lat,
            'longitud':long,
            'cliente':id_cliente
          }
          $http({
            method:'get',
            //url:'http://172.20.16.177/agentes/public'+'/guardar-datos',
            //url:'http://192.168.1.67:8888/corporativo/agentes/public'+'/guardar-datos',
            url:'http://185.3.166.10:80/agentes/public'+'/guardar-datos',
            //url:'http://172.20.16.159:8888/agentes/public'+'/guardar-datos',
            //url:'http://5.10.26.10/agentes/public/'+'/guardar-datos',
            params:json
          }).success(function (res) {
            $scope.id_cliente=res;
            $scope.data=res;
            var respuesta="Visita generada exitosamente.";
            var alert=$ionicPopup.alert({
              tittle:'respuesta',
              template:respuesta
            })
          })
        })
      })
    }


    $scope.enviarCorreo=function () {
      var cliente=$scope.id_cliente['cliente_id'];
      json={
        'cliente':cliente
      }
      $http({
        method:'get',
        url:'http://185.3.166.10:80/agentes/public'+'/enviar_correo',
        //url:'http://172.20.16.159:8888/corporativo/agentes/public'+'/enviar_correo',
        //url:'http://172.20.16.177/agentes/public'+'/enviar_correo',
        //url:'http://5.10.26.10/agentes/public'+'/enviar_correo',
        //url:'http://172.20.16.177/agentes/public/'+'/enviar_correo',
        params:json
      }).success(function (res) {
        console.log(res);
      });
    }


  $scope.obtenerId=function () {
    var identificador = $cordovaDevice.getUUID();
    var alert=$ionicPopup.alert({
     tittle:'Identificador',
     template:identificador
    })
  }
});
