$(document).on("ready", function(){
        var map = new GMaps({
            div: '#map',
            lat: $("#lat").val(),
            lng: $("#lng").val(),
            zoom:11
        });

    map.addMarker({
        lat: $("#lat").val(),
        lng: $("#lng").val(),
        draggable: true,
        dragend: function(e) {
            var position = map.markers[0].getPosition();
            lat = position.lat();
            lng = position.lng();
            $("#lat").val(lat);
            $("#lng").val(lng);
        }
    });

        $('#search').click(function(e){
            e.preventDefault();
            map.removeMarkers();
            GMaps.geocode({
                address: $('#direccion').val().trim(),
                callback: function(results, status){
                    if(status=='OK'){
                        var latlng = results[0].geometry.location;
                        map.setCenter(latlng.lat(), latlng.lng());
                        map.addMarker({
                            lat: latlng.lat(),
                            lng: latlng.lng(),
                            draggable: true,
                            dragend: function(e) {
                                var position = map.markers[0].getPosition();
                                lat = position.lat();
                                lng = position.lng();
                                $("#lat").val(lat);
                                $("#lng").val(lng);
                            }
                        });
                        $("#lat").val(latlng.lat());
                        $("#lng").val(latlng.lng());
                    }
                }
            });
        });

});